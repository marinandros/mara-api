package types

type Pokemon struct {
	Name   string `json:"Title"`
	Desc   string `json:"desc"`
	Attack int    `json:"content"`
}

type Pokemons []Pokemon

type PokemonList struct {
	Count    int         `json:"count"`
	Next     string      `json:"next"`
	Previous interface{} `json:"previous"`
	Results  []struct {
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"results"`
}
