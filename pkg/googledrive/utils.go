package googledrive

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/spf13/viper"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
)

func generateConfig() *oauth2.Config {
	byteCreds := generateCredentials()

	config, err := google.ConfigFromJSON(byteCreds, drive.DriveMetadataReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}

	return config
}

func getClient(config *oauth2.Config) *http.Client {
	tok, err := generateToken()

	if err != nil {
		fmt.Println(err)
		tok = getTokenFromWeb(config)
		storeToken(tok)
	}
	return config.Client(context.Background(), tok)
}

func generateCredentials() []byte {
	clID := viper.GetString("CLIENT_ID")
	clSecret := viper.GetString("CLIENT_SECRET")
	projID := viper.GetString("PROJECT_ID")
	authURL := viper.GetString("AUTH_URL")
	tokenURL := viper.GetString("TOKEN_URL")
	authProvider := viper.GetString("AUTH_PROVIDER_x509_CERT_URL")
	redirectUris := strings.Split(viper.GetString("REDIRECT_URIS"), ";")

	return []byte(fmt.Sprintf(`{"installed": { 
		"client_id": "%s", 
		"project_id": "%s", 
		"auth_uri": "%s", 
		"token_uri": "%s", 
		"auth_provider_x509_cert_url": "%s", 
		"client_secret": "%s", 
		"redirect_uris": [ "%s", "%s"] 
	}}`, clID, projID, authURL, tokenURL, authProvider, clSecret, redirectUris[0], redirectUris[0]))
}
