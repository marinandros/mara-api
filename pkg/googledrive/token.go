package googledrive

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

func generateToken() (*oauth2.Token, error) {
	accessToken := viper.GetString("ACCESS_TOKEN")
	tokenType := viper.GetString("TOKEN_TYPE")
	refreshToken := viper.GetString("REFRESH_TOKEN")
	expiry := viper.GetString("EXPIRY")

	tokenReader := strings.NewReader(fmt.Sprintf(`{
			"access_token": "%s",
			"token_type": "%s",
			"refresh_token": "%s",
			"expiry": "%s"
	}`, accessToken, tokenType, refreshToken, expiry))

	tok := &oauth2.Token{}
	err := json.NewDecoder(tokenReader).Decode(tok)
	return tok, err
}

func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	fmt.Println(fmt.Sprintf("%s", viper.GetString("ACCESS_TOKEN")))
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web %v", err)
	}
	return tok
}

func storeToken(token *oauth2.Token) {
	fmt.Println(token)
	viper.Set("ACCESS_TOKEN", token.AccessToken)
	viper.Set("TOKEN_TYPE", token.TokenType)
	viper.Set("REFRESH_TOKEN", token.RefreshToken)
	viper.Set("EXPIRY", (token.Expiry).Format(time.UnixDate))
}
