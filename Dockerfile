FROM golang:alpine

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
# COPY go.mod .
# COPY go.sum .
# RUN go mod download

# Copy the code into the container
# COPY . .

# Build the application
# RUN go build -o main .
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...


# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
# RUN cp /build/cmd/importer/main .

# Export necessary port
ENV PORT 3000
EXPOSE 3000

# Command to run when starting the container
CMD ["api"]